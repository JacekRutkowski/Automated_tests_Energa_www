from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from webdriver_manager.chrome import ChromeDriverManager


def test_titles():
    driver = webdriver.Chrome(ChromeDriverManager().install())
    driver.maximize_window()
    driver.get("https://grupa.energa.pl/")
    print(driver.title)
    try:
        assert'Grupa Energa - wytwarzanie, obrót i dystrybucja energii elektrycznej' in driver.title
        print('Assertion test pass')

    except Exception as e:
        print('Assertion test failed', format(e))


    driver.find_element(By.XPATH, "//form[@class='form-form']//input").send_keys("Kariera")
    driver.find_element(By.XPATH, "//form[@class='form-form']//input").send_keys(Keys.ENTER)
    print(driver.title)


    driver.find_element(By.XPATH, "//div[3]/a").click()
    print(driver.title)
    try:
        assert'Kariera w Grupie Energa' in driver.title
        print('Assertion test pass')

    except Exception as e:
        print('Assertion test failed', format(e))

    driver.find_element(By.XPATH, "//div[2]/div[3]/div/div/a").click()
    print(driver.title)
    try:
        assert'Dołącz do nas | Grupa Energa' in driver.title
        print('Assertion test pass')

    except Exception as e:
        print('Assertion test failed', format(e))

    driver.find_element(By.XPATH, "//div[2]/div/div/div[3]/a").click()
    print(driver.title)
    try:
        assert'Oferty pracy w Grupie Energa' in driver.title
        print('Assertion test pass')

    except Exception as e:
        print('Assertion test failed', format(e))

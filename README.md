# Automated tests website ENERGA

## Description
Test project for getting to know Git with Python and PyCharm and creating simple automated tests

## Author
Author: ***Jacek Rutkowski***

## Technologies
- Git 2.26.0
- PyCharm 2019.3.3(Community Editon)
- Python 3.8.1
- Windows 10 x64

